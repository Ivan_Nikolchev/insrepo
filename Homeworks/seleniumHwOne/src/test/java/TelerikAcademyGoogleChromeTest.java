import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TelerikAcademyGoogleChromeTest {

    @Test
    public void navigateTelerikAcademyByChrome(){
        System.setProperty("webdriver.chrome.driver","D:\\Java Projects\\ChromeDrivers\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.get("https://www.google.com/search?q=Telerik+academy+alpha&source=hp&ei=YgvhYqDpEbi9xc8Pt-uWcA&iflsig=AJiK0e8AAAAAYuEZcqadpbIamEtimTvbg5leZjnv1QVZ&ved=0ahUKEwigotK_5pj5AhW4XvEDHbe1BQ4Q4dUDCAc&uact=5&oq=Telerik+academy+alpha&gs_lcp=Cgdnd3Mtd2l6EAMyBQgAEIAEMgUIABCABDIICAAQgAQQyQMyBggAEB4QFjoLCAAQgAQQsQMQgwE6BAgAEEM6EQguEIAEELEDEIMBEMcBENEDOggIABCABBCxAzoRCC4QgAQQsQMQgwEQxwEQrwE6DQgAEIAEELEDEIMBEAo6CwguEIAEEMcBENEDOgoILhCABBDUAhAKOgsILhCABBDHARCvAToICAAQHhAPEBZQlQRY5TBg6DFoAXAAeACAAawBiAGND5IBBDE2LjWYAQCgAQGwAQA&sclient=gws-wiz");
        webDriver.findElement(By.id("L2AGLb")).click();
        String inputSearch = webDriver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/div/a/h3")).getText();


        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha",inputSearch);
        String expectedString = "IT Career Start in 6 Months - Telerik Academy Alpha";
        if(inputSearch.equalsIgnoreCase(expectedString))
            System.out.printf("The title '%s' is the first search result by 'Google'",inputSearch);
        else
            System.out.println("Title didn't match");
        webDriver.quit();
    }




}